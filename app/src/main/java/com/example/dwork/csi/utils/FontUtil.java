package com.example.dwork.csi.utils;

import android.content.Context;
import android.graphics.Typeface;


/**
 * Created by Korzch Alexandr on 3/13/2016.
 */
public class FontUtil {


    private static Typeface robotoRegular;
    private static Typeface robotoMedium;
    private static Typeface robotoBold;


    public static Typeface getRobotoRegularTypeFace(Context context) {
        if (robotoRegular == null) {
            robotoRegular = Typeface.createFromAsset(context.getAssets(), "fonts/Roboto-Regular.ttf");
        }
        return robotoRegular;
    }


    public static Typeface getRobotoMediumTypeFace(Context context) {
        if (robotoMedium == null) {
            robotoMedium = Typeface.createFromAsset(context.getAssets(), "fonts/Roboto-Medium.ttf");
        }
        return robotoMedium;
    }


    public static Typeface getRobotoBoldTypeFace(Context context) {
        if (robotoBold == null) {
            robotoBold = Typeface.createFromAsset(context.getAssets(), "fonts/Roboto-Bold.ttf");
        }
        return robotoBold;
    }
}
